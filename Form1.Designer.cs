﻿namespace project1_2023
{
    partial class samyProject
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.firstProject = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // firstProject
            // 
            this.firstProject.Location = new System.Drawing.Point(318, 176);
            this.firstProject.Name = "firstProject";
            this.firstProject.Size = new System.Drawing.Size(131, 81);
            this.firstProject.TabIndex = 0;
            this.firstProject.Text = "Ricard Samuel Michel";
            this.firstProject.UseVisualStyleBackColor = true;
            this.firstProject.Click += new System.EventHandler(this.firstProject_Click);
            // 
            // samyProject
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.firstProject);
            this.Name = "samyProject";
            this.Text = "Samy Project";
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Button firstProject;
    }
}

